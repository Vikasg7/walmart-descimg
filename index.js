(function (require, module) {

   var _ = require("./js/functions.js")
   var $ = require("streamify")
   var P = require("promise")
   var R = _.Request
   var format = require("util").format
   var Parse = JSON.parse
   var delayInSec = 1
   // var proxy = _.proxy

   process.stdin
      .setEncoding("UTF-8")
      .pipe($(_.readline))
      .pipe($(getDescImg))
      .on("finish", function () { console.error("Done!") })

   var i = 0
   function getDescImg(url, enc, getNextUrl) {
      console.error(i++, url)
      return (
      P.resolve(url)
         .then(makeRequest)
         .then(parseData.bind(null, url))
         .catch(errHandler.bind(null, url))
         .then(wait)
         .then(getNextUrl)
      )
   }

   function makeRequest(url) {
      return (
      R({method: "GET", uri: url, proxy: null/*proxy.getProxy()*/})
         .catch(function (error) {
            if (error == 520) {
               return R({method: "GET", uri: url, proxy: null/*proxy.getProxy()*/})
            } else {
               throw error
            }
         })
      ) 
   }

   function parseData(url, body) {
      var masterMap
      try {
         masterMap = Parse((body.match(/window.__WML_REDUX_INITIAL_STATE__ = (.*?);};<\/script>/) || 
                            body.match(/<script id="content" type="application\/json">(.*?)<\/script>/))[1])
      } catch (e) {
         throw format("masterMap not found") 
      }

      var productId = masterMap.product.selected.product

      var imagesMap = masterMap.product.images
      var productsMap = masterMap.product.products
      var product = productsMap[productId]

      if (product.images) {
         var p = {}
         p.url = url
         p.productId = productId
         try { p.image = (imagesMap[product.images[0]].assetSizeUrls.main || "").match(/(.*?)\?/)[1] } catch (e) { p.image = "N/A" }
         // var pa = product.productAttributes
         // p.shortDesc = (pa.shortDescription || "N/A").replace(/"|\n|\r/g, "")
         // p.medDesc = (pa.mediumDescription || "N/A").replace(/"|\n|\r/g, "")
         // p.longDesc = (pa.detailedDescription || "N/A").replace(/"|\n|\r/g, "")
         // try { p.category = (pa.productCategory.categoryPath || "N/A").replace(/"/g, "") } catch (e)  { p.category = "" }
         console.log('"' + getValues(p).join('","') + '"')
      } else {
         throw "info not found"
      }
   }

   function errHandler(url, error) {
      console.error(url, "-", error.toString())
      // console.error(error)
   }

   function wait() {
      return new P(function (resolve, reject) {
         setTimeout(resolve, delayInSec * 1000)
      })
   }

   // Anciliary functions
   function getValues(object) {
      return Object.keys(object).map(function (key) { return object[key] })
   }


})(require, module)
