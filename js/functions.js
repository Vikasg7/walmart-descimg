(function (require, module) {

   var headers = {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36",
      "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "accept-encoding": "gzip, deflate, sdch, br",
      "accept-language": "en-US,en;q=0.8"
   }

   var promise = require("promise")
   var Session = require("request").defaults({/*jar: true, */gzip: true, headers: headers})

   readFileSync = require("fs").readFileSync

   module.exports = {
      readline: {
         options: {
            objectMode: true
         },
         transform: function (data, enc, done) {
            var stream = this
            var lines = [stream.lastLine, data].join("").split(/\r?\n|\r(?!\n)/)
            stream.lastLine = lines.pop()
            lines.length && lines.forEach(function (line) { line && this.push(line) }, stream)
            done()
         },
         flush: function (done) {
            var stream = this
            if (stream.lastLine) { 
               stream.push(stream.lastLine)
               stream.lastLine = null
            }
            done()
         }
      },

      Request: function (options) {
         return new promise(function (resolve, reject) {
            Session(options, function (err, resp, body) {
               if (err) {
                  reject(err.code)
               } else if ((resp.statusCode !== 200) && (resp.statusCode !== 201)) {
                  reject(resp.statusCode)
               } else {
                  resolve(body)
               }
            })
         })
      },

      proxy: {
         proxies: readFileSync("./proxies/proxies.txt", "UTF-8").toString().trim().split(/\r\n|\n/),

         proxiesCopy: [],

         getProxy: function () {
            if (!this.proxiesCopy.length) { this.proxiesCopy = this.proxies.slice(0) }
            return this.proxiesCopy.shift()
         }
      }
   }

})(require, module)